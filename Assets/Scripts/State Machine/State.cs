using System;
using UnityEngine;

public abstract class State
{
	protected StateMachine stateManager;
	protected TurnManager turnManager;
	public AbsCharacter selectedCharacter{
		get{ 
			return stateManager.SelectedCharacter;
		} 
		set{
			stateManager.SelectedCharacter = value;
		}
	}


	public State(StateMachine stateManager){
		this.stateManager = stateManager;
		this.turnManager = TurnManager.GetTurnManager();
	}
/*
	public virtual void OnButtonClick(string buttonName){
		Attack currentAttack = selectedCharacter.InstantiateAttack(buttonName);
		stateManager.SwitchState(new CharacterAttacking(stateManager, currentAttack));
	}
*/
	public abstract void OnTileClick(Tile tilePressed);
	public abstract void OnCharacterClick(AbsCharacter characterPressed);


	// Optional Methods:
	public virtual void OnStateEnter(){

	}

	public virtual void OnStateExit(){

	}

	protected void Log(object message){
		Debug.Log("<color=orange><b>"+GetStateName()+" : </b></color>"+message+"");
	}

	protected abstract string GetStateName();

}

