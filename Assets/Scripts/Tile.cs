﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
public enum Highlight{
	Default=0, Move=1, Attack=2
}
*/

public struct HighlightColors{
	//Don't use the default. That's too rigid, new hex, you have to change this
	public static Color Default = Color.white;

	public static Color Move = Color.cyan;
	public static Color Attack = Color.red;
		
};

public class Tile : MonoBehaviour {

	//	public int x; public int y; public int z; //Used for Unity in-editor debugging
	public bool mouseOverCoordinates=true;

	public GameObject CharacterOnTile {get; set;}
	public Attack AttackOnTile {get;set;}
	public Color CurrentHighlight{get;set;}

	private bool _passable;
	private Color defaultColor;
	private Material material;
	private float tileWidth;
	private float tileHeight;
	private int sortingOrder;

	// Axial coordinates is a system of storing hexagons that uses 3 axis. The third Axis is used for computation only. 
	// http://www.redblobgames.com/grids/hexagons/#coordinates
	
	//Axial Coordinates
	public int X {get;set;} 
	public int Y {get;set;} 	
	public int Z {get;set;}
	
	public int Height {get;set;} //Tile height or level.
	public bool Movable {get;set;}
	public bool Passable {
		get{ return _passable; }
		set{
			_passable=value;
			if(!_passable){
				CurrentHighlight = defaultColor = material.color = Color.gray;	//TODO: remove this line when adding obstacles
				Movable=false;
			}
		} 
	}


	void Awake() {
		this.gameObject.layer = LayerMask.NameToLayer("HexTiles");

		CharacterOnTile = null;
		material = this.GetComponent<Renderer>().material;
		defaultColor = HighlightColors.Default;
		CurrentHighlight = defaultColor;

		SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
		sortingOrder = spriteRenderer.sortingOrder;
		tileWidth = (spriteRenderer.bounds.size.x);
		tileHeight = (spriteRenderer.bounds.size.x);
		Passable=true;	//TODO: When bringing obstacles in, this line will have to be changed
		Movable=true;	//TODO: When bringing obstacles in, this line will have to be changed
	}

	void Start () {

	}

	void OnMouseEnter () {
		if(mouseOverCoordinates) {
			Debug.Log ("x: "+X+"| y: "+Y);
		}
		this.GetComponent<Renderer>().material.color = Color.yellow;
	}

	void OnMouseExit(){
		this.GetComponent<Renderer>().material.color = CurrentHighlight;
	}

	public static int GetDistance(Tile start, Tile goal) {
		return (Mathf.Abs(start.X - goal.X) + Mathf.Abs(start.Y - goal.Y) + Mathf.Abs(start.Z - goal.Z)) /2;
	}

	public Vector3 GetCoordinates(){
		return new Vector3(X, Y, Z);
	}

	public void StoreAxialCoordinates(int x, int y) {
		X = x;
		Y = y;
		Z = -x-y;
	}

	// Method for positioning at the beginning of the fight
	public void PlaceCharacterOnTile(GameObject gameObj) {
		Vector3 onTilePosition = GetCenteredPosition();

		CharacterOnTile = gameObj;
		Character characterScript = gameObj.GetComponent<Character>();

		gameObj.transform.position = onTilePosition;
		gameObj.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder + 1;
		gameObj.GetComponent<Character>().CurrentTile = this;
		Movable=false;
	}

	public Vector3 GetCenteredPosition(){
		Vector3 centeredPosition = this.transform.position;
		centeredPosition.y += tileHeight/4 ;
		return centeredPosition;

	}

	public void Highligth(Color highlightToTurnOn){
		//material.color = highlightToTurnOn;
		this.GetComponent<Renderer>().material.color = highlightToTurnOn;
		CurrentHighlight = highlightToTurnOn;
	}

	public void HighlightOff(){
		// material.color = defaultColor;
		this.GetComponent<Renderer>().material.color = defaultColor;
		CurrentHighlight = HighlightColors.Default;
	}

	public override string ToString(){
		return "X: "+X+", Y:"+Y;
	}
}
