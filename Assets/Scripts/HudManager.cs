﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HudManager : MonoBehaviour {

	//Lazy Singleton
	private static HudManager hudManager;
	public static HudManager GetHudManager(){
		return hudManager;
	}

	public Canvas attackCanvas;
	public Canvas defenseCanvas;

	public delegate void ButtonClicked(string buttonName); 
	public static event ButtonClicked OnButtonClick;

	private List<Button> attackCanvasButtons;
	private List<Button> defenseCanvasButtons;
	private GameManager gameManager;

	void Awake(){
		hudManager = this;
		SetButtonsReferencesAndAddListeners ();

	//	SetCanvasReferences ();
	}

	void Start(){
		gameManager = GameManager.GetGameManager();
	}

	public void ShowAttackHUD(bool enable){
		attackCanvas.gameObject.SetActive(enable);
	}
	
	public void OnButtonPressed(string buttonName){
		gameManager.ButtonPressed(buttonName);
		/*
		if(OnButtonClick!=null){
			OnButtonClick(buttonName);
		}
		*/
	}

	void SetButtonsReferencesAndAddListeners (){
		attackCanvasButtons = new List<Button>();
		defenseCanvasButtons = new List<Button>();
		foreach (Transform current in attackCanvas.transform) {
			Button currentButton = current.GetComponent<Button> ();
			if (currentButton != null) {
				attackCanvasButtons.Add (currentButton);
				currentButton.onClick.AddListener ( delegate{ OnButtonPressed( currentButton.GetComponentInChildren<Text>().text ); } );
			}
		}
		foreach (Transform current in defenseCanvas.transform) {
			Button currentButton = current.GetComponent<Button> ();
			if (currentButton != null) {
				defenseCanvasButtons.Add (currentButton);
				currentButton.onClick.AddListener ( delegate{ OnButtonPressed( currentButton.GetComponentInChildren<Text>().text ); } );
			}
		}
	}

	/*
	public void ButtonIsPressed(){
		gameManager.AttackButtonPressed(gameObject.name);
	}
	*/
}


/*
 * 
	public void CharacterPressed(Character character){
		//TODO: Migliorare questa funzione, fare un controllo sul turno. Oppure nel turno avversario togli il mouse. Vedi te
		//		Non lasciare che questa funzione venga chiamata quando il turno non è del giocatore.
		SelectedChar = character; 
	}
	
	void Update(){
		if(SelectedChar==null){ return; }
		
		if(Input.GetMouseButtonDown(0)){
			MoveToTile ();
		}
		
	}
	
	void MoveToTile (){
		Tile tileHit = RayCameraToMouse (tileMask).gameObject.GetComponent<Tile> ();
		if (tileHit == null) {
			return;
		}
		SelectedChar.Move (tileHit);

	}
*/


	/*
	void SetCanvasesReference (){
		foreach (Transform child in transform) {
			if (child.name.Contains ("Attack")) {
				attackCanvas = child.GetComponent<Canvas>();
			}
			if (child.name.Contains ("Defense")) {
				defenseCanvas = child.GetComponent<Canvas>();
			}
		}
	}
*/


