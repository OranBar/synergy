using UnityEngine;
using System;
using System.Collections;

public class CharacterSelected : State {

	private HudManager hudManager;
	private Grid grid;
	private BattleActionFactory actionsFactory;	//TODO: assign the factory.

	public CharacterSelected(StateMachine stateManager) : base(stateManager){
		hudManager = HudManager.GetHudManager();
		grid = GameObject.FindObjectOfType<Grid>();
		actionsFactory = GameObject.FindObjectOfType<BattleActionFactory>();
	}

	public override void OnTileClick(Tile tilePressed){
		try{
			/* Now that we have battleActions, this line becomes
			selectedCharacter.Move(tilePressed);
			*/

			//selectedCharacter.DoAction( new MoveAction(tilePressed) );
		} catch {
			return;
		}
		//Todo: This has to be done in BattleAction, not here. State Machine knows nothing about actions
		turnManager.DecreaseRemainingActions(); 
		stateManager.SwitchState(new PlayerTurn(stateManager));
	}

	public override void OnCharacterClick(AbsCharacter characterPressed){
		selectedCharacter = characterPressed;
		OnStateEnter();
	}

	public override void OnStateEnter (){

		//Do i have to pass the stateManager??
		Log("State Enter - Do Action");
		//selectedCharacter.DoAction( actionsFactory.CreateBattleAction("Move", selectedCharacter) );
		// selectedCharacter.DoAction( new MoveAction(selectedCharacter, stateManager) );


	//	selectedCharacter.ShowMovableTiles();
		hudManager.ShowAttackHUD(true);
	}

	public override void OnStateExit (){
		//	grid.HighlightsOff();
		//TODO: Do this in EndAction.
		hudManager.ShowAttackHUD(false);
	}

	protected override string GetStateName ()
	{
		return "CharacterSelected";
	}
}
