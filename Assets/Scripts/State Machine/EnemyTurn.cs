using UnityEngine;
using System.Collections;

public class EnemyTurn : State {

	private Ai ai;

	public EnemyTurn(StateMachine stateManager) : base(stateManager){
//		bool startTurn;
		//TODO: wait a few seconds
		ai = GameObject.FindObjectOfType<AiBasic>();;
	}

	public override void OnStateEnter() {

		ai.ExecuteTurn( delegate(){ stateManager.SwitchState(new PlayerTurn(stateManager)); } );



		//Should be doing it here, but I need to make sure it happens after the ExecuteTurn method is done
		stateManager.SwitchState(new PlayerTurn(stateManager));

	}

	public override void OnTileClick(Tile tilePressed){
		
	}
	
	public override void OnCharacterClick(AbsCharacter characterPressed){

	}

	//TODO: remove this
	public override void OnStateExit(){
		GameObject.FindObjectOfType<Grid>().HighlightsOff();
	}

	protected override string GetStateName ()
	{
		return "EnemyTurn";
	}


}

