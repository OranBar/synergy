using System;

public interface IBattleActionFactory {

	BattleAction CreateBattleAction(string actionName, AbsCharacter actingCharacter);

}


