using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AiBasic : Ai {

	private int actionsPerformed;
	private CharacterAiBasic selectedCharacter;
	private Attack selectedAttack;
	private GameObject target;


	public override void Start (){
		base.Start();
		actionsPerformed = 0;
		selectedCharacter = null;
	}

	//TODO: remove the highlightsOff. 
	public override IEnumerator ExecuteMove (){
		Tile moveTile = null;
		grid.HighlightsOff();


		yield return new WaitForSeconds(1f);

		selectedCharacter = SelectNextCharacter();
		selectedAttack = SelectAttack();
		SelectMoveTileAndTarget(out moveTile, out target);
		selectedCharacter.Move(moveTile);
		turnManager.DecreaseRemainingActions();

		yield return new WaitForSeconds(1f);

		//TODO: Since the refactoring, characters do not have methods for attacking a character anymore.
		//selectedCharacter.AttackCharacter(selectedAttack, target.GetComponent<Character>());

		// The TurnManager shouldn't be updated by this class directly
		turnManager.DecreaseRemainingActions();

		grid.HighlightsOff();
		actionsPerformed++;
		yield return null;
	}

	private CharacterAiBasic SelectNextCharacter(){
		int nextCharacterIndex = actionsPerformed%(characters.Length);
		GameObject nextCharacter = characters[nextCharacterIndex];
		return nextCharacter.GetComponent<CharacterAiBasic>();
	}

	private Attack SelectAttack(){
		Attack selectedAttack;
		do{
			selectedAttack = selectedCharacter.GetRandomAttack();
		} while(!selectedAttack.targetsCharacters);

		return selectedAttack;
	}

	private void SelectMoveTileAndTarget(out Tile moveTile, out GameObject target){
		moveTile = null;
		target = null;
		List<Tile> movableTiles = grid.DjikstraRadius(selectedCharacter.CurrentTile, selectedCharacter.MoveRadius);
		
		foreach (Tile currentTile in movableTiles ){
			List<Tile> tilesInRange = grid.RadiusTiles(currentTile, selectedAttack.range);
			
			for(int i=0; i<tilesInRange.Count; i++){
				if(tilesInRange[i].CharacterOnTile != null && tilesInRange[i].CharacterOnTile != selectedCharacter.gameObject){
					target = tilesInRange[i].CharacterOnTile;
					moveTile = currentTile;
					return;
				}
			}
		}
	}


/*
	private GameObject FindCloseTarget(Tile tile){
		GameObject target = null;
	//	int attackRange = selectedCharacter.moveRadius + selectedAttack.range;
	//	List<Tile> tilesInRange = grid.RadiusTiles(selectedCharacter.CurrentTile.AttackOnTile, attackRange);

		List<Tile> tilesInRange = grid.RadiusTiles(tile, selectedAttack.range);


		for(int i=0; i<tilesInRange.Count; i++){
			if(tilesInRange[i].CharacterOnTile != null){
				target = tilesInRange[i].CharacterOnTile;
				break;
			}
		}
		return target;
	}
*/


}

