﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OranUnityUtils;


public class SpriteController : MonoBehaviour {

	public float moveSpeed=5f;

	private Grid grid;

	void Start(){
		grid = GameObject.FindObjectOfType<Grid>();
	}

	public void Move(List<Tile> path){
		StartCoroutine( Move_Coro(path, moveSpeed) );
	}

	
	public void Move(List<Tile> path, float speed ){
		StartCoroutine( Move_Coro(path, speed) );
	}

	public IEnumerator Move_Coro(List<Tile> path, float speed){
		foreach(Tile currentTarget in path){

			Vector3 destination = currentTarget.transform.position;  //Useless line?

			if(currentTarget!=null){ destination = currentTarget.GetCenteredPosition(); }

			while(Vector2.Distance(transform.position, destination)>=0.01f){
				transform.position = Vector2.MoveTowards(transform.position,destination, speed*Time.deltaTime);
				yield return null;
			}
		}
	//	grid.HighlightsOff();
	}

	public IEnumerator Move(List<Tile> path, float speed){
		foreach(Tile currentTarget in path){
			
			Vector3 destination = currentTarget.transform.position;  //Useless line?
			
			if(currentTarget!=null){ destination = currentTarget.GetCenteredPosition(); }
			
			while(Vector2.Distance(transform.position, destination)>=0.01f){
				transform.position = Vector2.MoveTowards(transform.position,destination, speed*Time.deltaTime);
				yield return null;
			}
		}
		grid.HighlightsOff();
	}


	//TODO
	public IEnumerator StartAnimation(Attack attack){
		attack.GetComponent<Animation>().Play();  //TODO: fix this line
		yield return null;
	}
}
