﻿using UnityEngine;
using System.Collections;

public class LifePositioner : MonoBehaviour {

	public Transform playerPosition;
	public Vector2 offsets = new Vector2(0.0f, 0.5f);
	public float smoothing=5f;

	void FixedUpdate(){
		Vector3 barOffset = new Vector3();
		barOffset.Set(offsets.x, offsets.y, 1);
		Vector3 newPosition = playerPosition.position + barOffset;
		newPosition.z = 1;
		transform.position = Vector3.Lerp(transform.position, newPosition, smoothing);
	}

}
