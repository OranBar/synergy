using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ai : MonoBehaviour {

	protected TurnManager turnManager;
	protected GameObject[] characters;
	protected Grid grid;

	public virtual void Start(){
		characters = null;
		turnManager = TurnManager.GetTurnManager();
		characters = GameObject.FindGameObjectsWithTag("Enemy");
		grid = GameObject.FindObjectOfType<Grid>();
	}

	public void ExecuteTurn(Action executeAsLastLine){
		StartCoroutine( ExecuteTurn_Coro(executeAsLastLine) );
	}

	public IEnumerator ExecuteTurn_Coro(Action executeAsLastLine){
		Debug.Log("Mehere");
		int i=0;

		while(turnManager.CurrentTurn == Turn.Enemy){
			i++;
			yield return StartCoroutine( ExecuteMove() );
		}
//		executeAsLastLine();
	} 

	public abstract IEnumerator ExecuteMove();

}


