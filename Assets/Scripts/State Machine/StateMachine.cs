﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour {

	public AbsCharacter SelectedCharacter {get;set;}
	public TurnManager turnManager;

	private State currentState;

	public void Start(){
//		turnManager = TurnManager.GetTurnManager();

		// Subscribe to event notifications
		//Debug.Log("StateMachine : Subscribing to Mouse Ray Events");
		Log("Subscribing to Mouse Ray Events");
		MouseRayEvents clickEvents = GameObject.FindObjectOfType<MouseRayEvents>();
		clickEvents.OnTileClick += OnTileClick;
		clickEvents.OnCharClick += OnCharacterClick;
//		HudManager.OnButtonClick += OnButtonClick;

		SwitchState( new PlayerTurn(this) );
	}

	public void SwitchState(State newState){
		/*
		//Not sure if it needs to be done here
		if(turnManager.RemainingActions <= 0){
			if(turnManager.CurrentTurn == Turn.Enemy){
				newState = new PlayerTurn(this);
				turnManager."(Turn.Player);
			}
			else if(turnManager.CurrentTurn == Turn.Player){
				newState = new EnemyTurn(this);
				turnManager."(Turn.Enemy);			
			}
		}
		*/
		if(currentState!=null){
			currentState.OnStateExit();
		}
		currentState = newState;
		Log("Current state is "+newState);
		currentState.OnStateEnter();
	}

	public void OnTileClick(Tile tilePressed){
		currentState.OnTileClick(tilePressed);
	}

	public void OnCharacterClick(AbsCharacter characterPressed){
		currentState.OnCharacterClick(characterPressed);
	}
	/*
	public void OnButtonClick(string buttonName){
		currentState.OnButtonClick(buttonName);
	}
	*/

	protected void Log(object message){
		Debug.Log("<color=purple><b>State Machine : </b></color>"+message+"");
	}

}
