using System;
using UnityEngine;

public abstract class BattleAction : MonoBehaviour {

	public int ActionCost {get; set;}
	protected AbsCharacter UsingCharacter{get;set;}


	public void Init(AbsCharacter user){
		UsingCharacter = user;
		InitImpl();
	}

	public abstract void InitImpl();

	public abstract void OnTileClick(Tile tile);
	public abstract void OnCharacterClick(AbsCharacter character);

	protected void EndAction(){
	//	Grid.GetGrid().HighlightsOff();
		TurnManager.GetTurnManager().DecreaseRemainingActions(ActionCost);
	}

	public abstract string GetActionName();
	protected abstract bool SameTypeAs(object o);
	protected abstract void Undo();

	public override string ToString() {
		return GetActionName();
	}

	public override bool Equals (object o){
		if( SameTypeAs(o) ){
			if(UsingCharacter == ((BattleAction)o).UsingCharacter){
				return true;
			}
		}
		return false;
	}

	protected void Log(object message){
		Debug.Log("<color=blue><b>"+GetActionName()+" : </b></color>"+message+"");
	}


}


