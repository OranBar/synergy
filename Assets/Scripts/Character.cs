using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using OranDataStructures;


public class Character : AbsCharacter {

	//public int moveRadius = 3;
	public int maxLife;
	public Slider hpBar;

	private int life;
	private SpriteController spriteController;
	private Grid grid;

	public Attack CurrentAttack{get;private set;}

	public List<Attack> usableAttacks;

	private HudManager hudManager;
	private GameManager gameManager;
	//TODO; Not sure Character should have an attackFactory. It should instead have an array of usable attacks
	private AttackFactory attackFactory;

	void Awake(){
		spriteController = GetComponent<SpriteController>();
//		attackFactory = new AttackFactory();
		attackFactory = (GameObject.Find("Attack Factory") as GameObject).GetComponent<AttackFactory>();
	}

	void Start(){
		grid = GameObject.FindObjectOfType<Grid>();
		hudManager = HudManager.GetHudManager();
		gameManager = GameManager.GetGameManager();
		if(gameManager==null) { Debug.Log("GameManager is null"); }
		life = maxLife;
		hpBar.maxValue = maxLife;
		hpBar.value = maxLife;
	}

	void Update(){

	}

	public void OnMouseDown(){
		if(hudManager==null){Debug.Log ("hudmanager is null");	}
		if(gameManager==null) { Debug.Log("GameManager is null"); }

//		hudManager.CharacterPressed(this);
//		gameManager.CharacterSelected();	//TODO: This should be done in MouseRayEvents
		gameManager.SelectedCharacter = this;
	}

	/*
	public override void ShowMovableTiles(){
		grid.ShowMovableTiles(CurrentTile, moveRadius);
	}
	*/

	public void Move(Tile targetTile){
		if (!(HighlightColors.Move.Equals(targetTile.CurrentHighlight)) ) {
			//TODO: Change the if check to something more relaiable than highlight tile. 
			// This way triggers the exception when enemy tries to move, since it does not highlight
			if(TurnManager.GetTurnManager().CurrentTurn == Turn.Player){
				throw new Exception("Tile not in movement range");
			}
			


//			return;
		}
		CurrentTile.CharacterOnTile = null;

		List<Tile> path = grid.A_StarPathfinding(CurrentTile, targetTile);
		spriteController.Move(path);

		CurrentTile = targetTile.GetComponent<Tile>();
		CurrentTile.CharacterOnTile = this.gameObject;
		CurrentTile.Movable = false;
	}

	// Don't need this anymore. It will be done by the BattleAction in the CharacterActing state.
	/*
	public void AttackCharacter(String selectedAttackName, Character target){
		Attack attackObj = this.InstantiateAttack(selectedAttackName) as Attack;
		attackObj.UseAttackOnCharacter(target);
	}

	public void AttackCharacter(Attack selectedAttackName, Character target){
		Attack attackObj = this.InstantiateAttack(selectedAttackName) as Attack;
		attackObj.UseAttackOnCharacter(target);
	}
	*/

	public Attack InstantiateAttack(string attackName){
		Attack attack = attackFactory.InstantiateAttack(attackName, this);
		return attack;
	}

	public Attack InstantiateAttack(Attack attackToInstantiate){
		Attack attack = attackFactory.InstantiateAttack(attackToInstantiate, this);
		return attack;
	}

	//TODO:
	public void Action(Attack attack, GameObject target){
		spriteController.StartAnimation(attack);
	}

	public override bool DecreaseLife(int damage){
		life = life - damage;
		hpBar.value = life;
		if(life<=0){
			Destroy(transform.parent.gameObject, 1f);
			//TODO: Character is dead. Be a little dramatic
			return true;
		}
		return false;
	}

	public override void DoAction(BattleAction action) {
		action.Init(this);
	}
}
