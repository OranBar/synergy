using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OranUnityUtils;

public class BattleActionComposite : BattleAction {

	private List<BattleAction> actionSequence = new List<BattleAction>();
	public float timeBetweenActions;

	public void AddBattleAction(BattleAction bAction){
		actionSequence.Add( bAction );
	}

	public void RemoveBattleAction(BattleAction bAction){
		for(int i=0; i<actionSequence.Count; i++){
			BattleAction action = actionSequence[i];
			if( action.Equals(bAction) ){
				actionSequence.RemoveAt(i);
			}
		}
	}

	public override void InitImpl ()
	{
		/*
		List<IEnumerator> actions = new List<IEnumerator>();

		foreach( BattleAction action in actionSequence ){
			actions.Add ( action.ActivateImpl().ToIEnum() );
			actions.Add ( new WaitForSeconds(timeBetweenActions) );
		}


		this.StartCoroutineTimeline( actions.ToArray() );
	*/
	}


	public override string GetActionName () {
		string result = "";
		foreach(BattleAction action in actionSequence){
			result = result + action.GetActionName() + " ";
		}
		return result;
	}

	public override bool Equals (object o) {
		BattleActionComposite param = o as BattleActionComposite;
		if(param != null){
			for(int i=0; i<actionSequence.Count; i++){
				if(actionSequence[i].Equals(param.actionSequence[i]) == false ){
					return false;
				}
			}
			return true;
		}
		return false;
	}



	public override void OnTileClick (Tile tile)
	{
		throw new System.NotImplementedException ();
	}

	public override void OnCharacterClick (AbsCharacter character)
	{
		throw new System.NotImplementedException ();
	}

	protected override bool SameTypeAs(object o) {
		throw new System.NotImplementedException ();
	}

	protected override void Undo ()
	{
		throw new System.NotImplementedException ();
	}
}

