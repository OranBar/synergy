﻿using UnityEngine;
using System.Collections;
using OranUnityUtils;

namespace OranUnityUtils
{
	public static class GameObjectExtensionMethods {
		
		public static void SetActiveAndParents(this GameObject go, bool activate){
			go.SetActive(activate);
			foreach(Transform transf in go.transform.GetParents()){
				transf.gameObject.SetActive(activate);
			}
		}
		
		public static void LookAt2D(this GameObject gameObj, Vector3 target){
			// LookAt 2D
			// get the angle
			Vector3 norTar = (target-gameObj.transform.position).normalized;
			float angle = Mathf.Atan2(norTar.y,norTar.x)*Mathf.Rad2Deg;
			// rotate to angle
			Quaternion rotation = new Quaternion ();
			rotation.eulerAngles = new Vector3(0,0,angle);
			gameObj.transform.rotation = rotation;
		}
	}
}