using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class BattleActionButtonListener : MonoBehaviour {

	private Button button;
	private IBattleActionFactory battleActionsFactory;


	public void Awake(){
		button = GetComponent<Button>();
		InitializeBattleActionFactory();
		button.onClick.AddListener( OnButtonClick );
	}

	public virtual void OnButtonClick(){
		AbsCharacter selectedCharacter = GameManager.GetGameManager().SelectedCharacter;
		string actionName = button.name;

		BattleAction action = battleActionsFactory.CreateBattleAction( actionName, selectedCharacter );
		selectedCharacter.DoAction( action );
	}

	protected virtual void InitializeBattleActionFactory(){
//		battleActionsFactory = GameObject.FindObjectOfType<BattleActionFactory>();
	}


}

