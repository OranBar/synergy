using System;
using UnityEngine;


public abstract class Attack : BattleAction{
	
	public int range = 3;
	public bool targetsCharacters, targetsTiles;
	
	public Character AttackingCharacter {get;set;}

	protected abstract void AwakeHook();
//	protected abstract void StartHook();
	
	private void Awake(){
		GetComponent<SpriteRenderer>().sortingLayerName = "Attacks";
		transform.position = Vector3.zero;
		Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), AttackingCharacter.GetComponent<Collider2D>());
		AwakeHook();
	}
	
	public abstract void HighlightAttackableSquares();
	protected abstract void AttackCharacter(AbsCharacter tile);
	protected abstract void AttackTile(Tile tile);
	
	public override void InitImpl () {
		HighlightAttackableSquares();
	}
	
	
	public sealed override void OnCharacterClick (AbsCharacter character) {
		if(!targetsCharacters){
			return;
		}
		this.AttackCharacter(character);
	}
	
	public sealed override void OnTileClick (Tile tile) {
		if(!targetsTiles){
			throw new Exception("This attack doesn't work on Tiles");
		}
		GetComponent<SpriteRenderer>().enabled = true;
		
		this.AttackTile(tile);
	}



	/*
	private void Start(){
		StartHook();
		// if(TurnManager.GetTurnManager().CurrentTurn == Turn.Player){
		HighlightAttackableSquares();    //TODO: this should be called by the player, not on start. This check is ugly
		// }
		
		this.gameObject.layer = LayerMask.NameToLayer("Attack");
	}
	 */
	/*
	public void UseAttackOnTile(Tile tile) {
		//		MouseRayEvents.OnTileClick -= UseAttackOnTile;
		if(!targetsTiles){
			throw new Exception("This attack doesn't work on Tiles");
		}
		GetComponent<SpriteRenderer>().enabled = true;
		
		this.AttackTile(tile);
	}
	
	public void UseAttackOnCharacter(Character character) {
		//		MouseRayEvents.OnCharClick -= UseAttackOnCharacter;
		if(!targetsCharacters){
			return;
		}
		GetComponent<SpriteRenderer>().enabled = true;
		
		AttackCharacter(character);
	}
*/


	
}


