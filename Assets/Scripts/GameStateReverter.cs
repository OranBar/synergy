using UnityEngine;
using System.Collections;
using OranUnityUtils;

public class GameStateReverter : MonoBehaviour
{
	/*
	Grid copyGrid;
	
	public void ScanGrid(){
		Grid grid = GameObject.FindObjectOfType<Grid>();
		for( int x = 0; x < grid.columns; x++ ){
			for( int y = 0; y < grid.rows; y++ ){
				Tile tile = grid.GetGridTile(x,y);
				if(tile.CharacterOnTile != null){
					
				}
			}
		}
	}

	public void CopyGrid(){
		Grid currentGrid = GameObject.FindObjectOfType<Grid>();
		copyGrid = currentGrid.Clone();
	}
*/

	private GameObject gameStateObj;
	private GameObject previousStateObj;

	public void SaveGameState(string currentStateObjName = "Current Game State"){
		previousStateObj = GameObject.Find(currentStateObjName);
		gameStateObj = Instantiate(previousStateObj);
		gameStateObj.SetActive (false);
	}

	public void SaveGameState(GameObject currentStateObj){
		previousStateObj = currentStateObj;
		gameStateObj = Instantiate(previousStateObj);
		gameStateObj.SetActive (false);
	}

	public void LoadSavedGame(){
		if(previousStateObj != null){
			previousStateObj.SetActive(false);
		}
		if(gameStateObj != null){
			gameStateObj.SetActive(true);
		}
	}

	public void RevertToOldState(){
		if(previousStateObj==null || gameStateObj==null){
			throw new UnityException("Cannot Revert: Save function was never called");
		}
		Destroy(gameStateObj);
		previousStateObj.SetActive(true);
	}
}

