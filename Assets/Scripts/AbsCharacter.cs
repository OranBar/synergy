using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbsCharacter : MonoBehaviour {

	public Tile CurrentTile{get;set;}

	public int MoveRadius;

//	public abstract void ShowMovableTiles();
//	public abstract void Move(Tile targetTile);
	public abstract void DoAction(BattleAction action);
	public abstract bool DecreaseLife(int damage);
}


