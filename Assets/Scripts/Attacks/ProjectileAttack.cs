using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ProjectileAttack : Attack {

	public int range = 3;
	public int damage;
	public float projectileForce = 50f;

	private GameObject target;
	private bool attacking;


	protected override void Init(){
	//	grid = Grid.GetGrid();
	//	Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), AttackingCharacter.GetComponent<Collider2D>());
	}

/*
	private void CreateProjectileOnSceneSetPosition(){
		Destroy(this.gameObject, 3.5f);
		this.gameObject.SetActive(true);
	}
*/
	void Update(){
		if(!attacking){ return; } 
	}

	public override void HighlightAttackableSquares(){
		List<Tile> tileList = grid.DjikstraRadius(AttackingCharacter.CurrentTile, range);
		grid.HighlightTiles(tileList, Highlight.Attack);
	}

	protected override void AttackTile(Tile tile){
	}
	 
	protected override void AttackCharacter(Character character){
		target = character.gameObject;
		this.transform.position = AttackingCharacter.transform.position;
		this.gameObject.LookAt2D(target.transform.position);
		Vector2 forceDirection = (target.transform.position - this.transform.position).normalized;
		this.GetComponent<Rigidbody2D>().AddForce(forceDirection * projectileForce);
		attacking = true;
	}

	public void OnCollisionEnter2D(Collision2D collisionInfo){
		Destroy(this.gameObject, 0.1f);
		Character characterCollided = collisionInfo.collider.GetComponent<Character>();
		if(characterCollided!=null){
			Debug.Log("mehere");
			characterCollided.DecreaseLife(damage);
		}
		Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), collisionInfo.gameObject.GetComponent<Collider2D>());
	}

	//TODO: The trigger should have a script with a method to change whatever passes through.
	public void OnTriggerEnter2D(Collider2D other){
		/*TODO: Find a way to understand what trigger triggered the method. Based on the trigger (es. Fire) and the attack(es. ArrowShot),
		 *	 	do different things.  */
		gameObject.GetComponent<SpriteRenderer>().color = Color.red;
	}


}
