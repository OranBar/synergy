﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
/*
// For Single Character Turns
public enum Turn{
	UserPlayer1=0, UserPlayer2=1, AIPlayer1=2, AIPlayer2=3
}
*/
public enum Turn{
	User, Ai
}


public class TurnManager : MonoBehaviour {

	//Singleton
	private static TurnManager turnManager;
	public static TurnManager GetTurnManager(){
		if(turnManager==null){
			turnManager = GameObject.FindObjectOfType<TurnManager>();
		}
		return turnManager;
	}

	public int totalUserActions=4, totalAiActions=4;

	private GridManager gridManager;

	public int userTeamActions;
	public int aiTeamActions=2;

	private List<Character> allActiveCharacters;
	private List<CharacterUser> userActiveCharacters;
	private List<CharacterAI> aiActiveCharacters;

	public GameObject playerPrefab1;
	public GameObject playerPrefab2;
	public GameObject aiPrefab;

	public Text playerActionsText;
	public Text enemyActionsText;

	public Turn turn {get;set;}

	public float secondiTraTurniAi = 4.0f;
	private List<CharacterAI> aiCharacters;


	void Awake(){
		allActiveCharacters = new List<Character>();
		userActiveCharacters = new List<CharacterUser>();
		aiActiveCharacters = new List<CharacterAI>();
		gridManager = GridManager.GetGridManager();
		userTeamActions=aiTeamActions=0;
		GameObject[] temp = (GameObject.FindGameObjectsWithTag("NPCEnemy"));
		aiCharacters = new List<CharacterAI>();
		foreach(GameObject current in temp){
			aiCharacters.Add(current.GetComponent<CharacterAI>());
		}

	}

	public void NextTurn(Character character){
		if(character is CharacterUser){
			DecreaseUserTeamActions();
		} else 
		if (character is CharacterAI){
			DecreaseAiTeamActions();
		}
	}

	public void FinishPlayerTurn(){
		GameManager.GetGameManager().ClearSelectedPlayerAndGUI();
		GridManager.GetGridManager().HighlightsOff();
		turn = Turn.Ai;
		UpdateTurnColors();
		UpdateCountText();
		aiTeamActions = totalAiActions;
		StartCoroutine(ExecuteAiTurn());
	}

	private void DecreaseUserTeamActions(){
		userTeamActions--;
		if(userTeamActions==0){ //User turn is over
			GameManager.GetGameManager().ClearSelectedPlayerAndGUI();

			UpdateTurnColors();

			if(turn==Turn.User){
				turn = Turn.Ai;
				aiTeamActions = totalAiActions;
				StartCoroutine(ExecuteAiTurn());
			}
		}
		UpdateCountText();
	}

	void UpdateTurnColors (){
		if(turn==Turn.User){
			enemyActionsText.color = Color.black;
			playerActionsText.color = Color.green;
		}
		if(turn==Turn.Ai){
			enemyActionsText.color = Color.red;
			playerActionsText.color = Color.black;
		} 
	}

	private void DecreaseAiTeamActions(){
		aiTeamActions--;
		if(aiTeamActions==0){ //User turn is over
			userTeamActions = totalUserActions;
			turn = Turn.User;
			UpdateTurnColors();
		}
		UpdateCountText();
	}

	IEnumerator ExecuteAiTurn(){
		yield return new WaitForSeconds(2.0f);
		while(aiTeamActions>=0){
			foreach(CharacterAI current in aiCharacters){
				current.ExecuteTurn();
				yield return new WaitForSeconds(secondiTraTurniAi);
			}
		}
	}

	private void UpdateCountText(){
		playerActionsText.text = "Player Actions: "+userTeamActions;
		enemyActionsText.text = "Enemy Actions: "+aiTeamActions;
	}

	#region Single Characters Turns
	/*
	public void NextTurn(Character character){
		if(character is CharacterUser){
			character.transform.renderer.material.color = Color.white;
		} else {
			character.transform.renderer.material.color = Color.blue;
		}
		int turnInt = (int)this.turn;
		this.turn = (Turn)((turnInt+1)%4);
		Debug.Log(turn);
		string characterName = turn.ToString ();
		GameObject nextCharacter = (GameObject.Find (characterName));
		EnableTurnCharacter (nextCharacter);
	}

	void EnableTurnCharacter (GameObject character)	{
		if(character.ToString().Contains("User")){
			character.transform.renderer.material.color = Color.green;
			// TODO: Tell that character(user) that it is his turn
		} else {
			character.transform.renderer.material.color = Color.green;
			// TODO: Tell that character(ai) that it is his turn
		}
	}

	// Start for single turns
	void Start () {
		turn = Turn.UserPlayer1;
	}
	*/
	#endregion

	public void CreatePlayers() {
		
		int middleX = (int)(Mathf.Floor(gridManager.rowLength/2));
		int middleY = (int)(Mathf.Floor(gridManager.columnLength/2));
		
		Tile spawnTile = gridManager.GetTile(middleX, middleY);
		Vector3 middleTilePosition = spawnTile.transform.position;
		
		CharacterUser userPlayer1 = playerPrefab1.GetComponent<CharacterUser>();
		userPlayer1.Init(middleX, middleY);
		userPlayer1.transform.position = middleTilePosition;
		spawnTile.movable = false;
		userPlayer1.currentTile = spawnTile;
//		userPlayer1.renderer.material.color = Color.green; // Linea usata con turni singoli
		userActiveCharacters.Add(userPlayer1);
		
		spawnTile = gridManager.GetTile(middleX-1, middleY);
		CharacterUser userPlayer2 = playerPrefab2.GetComponent<CharacterUser>();
		userPlayer2.Init((middleX-1), middleY);
		middleTilePosition.x -= 1;
		userPlayer2.transform.position = middleTilePosition;
		spawnTile.movable = false;
		userPlayer2.currentTile = spawnTile;
		userActiveCharacters.Add(userPlayer2);

		spawnTile = gridManager.GetTile(0,0);
		Vector3 bottomLeftTilePosition = spawnTile.transform.position;
		
		CharacterAI aiPlayer = aiPrefab.GetComponent<CharacterAI>();
		aiPlayer.Init(0,0);
		aiPlayer.transform.position = bottomLeftTilePosition;
		spawnTile.movable = false;
		aiPlayer.currentTile = spawnTile;
		
	}

	void Start(){
		userTeamActions = totalUserActions;
		playerActionsText.color = Color.green;
		aiTeamActions = 2;
		UpdateCountText();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
