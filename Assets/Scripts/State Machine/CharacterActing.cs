using UnityEngine;
using System.Collections;

public class CharacterActing : State {

	private BattleAction currentAction;
	private Grid grid;

	public CharacterActing(StateMachine stateManager, BattleAction attack) : base(stateManager){
		this.currentAction = attack;
		grid = GameObject.FindObjectOfType<Grid>();
	}

	public override void OnTileClick(Tile tilePressed){
		try{
			Log("Tile Click");
			currentAction.OnTileClick(tilePressed);
		} catch {
			return;
		}
		//Not here, should be done in the BattleAction class. A BattleAction may need more than 1 click to obtain all the information that it needs
	//	stateManager.SwitchState(new PlayerTurn(stateManager));
	}

	public override void OnCharacterClick(AbsCharacter characterPressed){
		try{
			Log("Character Click");
			currentAction.OnCharacterClick(characterPressed);
		} catch {
			return;
		}
		//Not here, should be done in the BattleAction class. A BattleAction may need more than 1 click to obtain all the information that it needs
	//	stateManager.SwitchState(new PlayerTurn(stateManager));
	}

	public override void OnStateEnter (){
		Log("State Enter");
//		currentAttack.HighlightAttackableSquares();
	}

	public override void OnStateExit (){
		//TODO: Take this out
		grid.HighlightsOff();
//		turnManager.DecreaseRemainingActions();
	}

	protected override string GetStateName ()
	{
		return "CharacterActing";
	}

}
