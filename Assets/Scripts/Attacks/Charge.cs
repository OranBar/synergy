﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Charge : Attack {

	public int range = 5;
	public float chargeSpeed = 20f;

	public override string ToString ()
	{
		return "Charge Attack";
	}

	protected override void Init() {

	}

	public override void HighlightAttackableSquares() {
		List<Tile> attackableTiles = grid.DjikstraRadius( AttackingCharacter.CurrentTile, range );
		grid.HighlightTiles( attackableTiles, Highlight.Attack );
	}

	protected override void AttackTile (Tile tile)
	{

	}

	protected override void AttackCharacter(Character tile) {

	}

	private void StartChargeAnimation(Tile tile){
		List<Tile> path = grid.A_StarPathfinding(AttackingCharacter.CurrentTile, tile);
		SpriteController attackingCharSpriteController = AttackingCharacter.GetComponent<SpriteController>();
		StartCoroutine( attackingCharSpriteController.Move(path, chargeSpeed) );
		
		AttackingCharacter.CurrentTile = tile;
		AttackingCharacter.CurrentTile.Movable = false;
	}
}

