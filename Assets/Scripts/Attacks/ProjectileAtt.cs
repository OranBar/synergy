using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OranUnityUtils;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]

public class ProjectileAtt : Attack {

	public int damage;
	public float projectileForce = 500f;
	public string attackName = "ChangeMe";

	protected Grid grid;
//	protected GameObject target;
	protected bool attacking;

	public override string GetActionName(){
		return attackName;
	}

	protected override void AwakeHook() {
		grid = FindObjectOfType<Grid>();
		if(GetComponent<Collider2D>()==null){
			Debug.LogError(this.gameObject.name+" has no collider");
		}
		GetComponent<Rigidbody2D>().gravityScale = 0f;
	}
	/*
	protected void StartHook() {
//		Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), AttackingCharacter.GetComponent<Collider2D>());
	}
*/

	public override void HighlightAttackableSquares() {
		List<Tile> tileList = grid.DjikstraRadius(AttackingCharacter.CurrentTile, range);
		grid.HighlightTiles(tileList, HighlightColors.Attack);
	}

	protected override void AttackTile(Tile tile) {

		//Unreachable Code since targetsTiles is false
	}
	 
	protected override void AttackCharacter(AbsCharacter character) {
		grid.HighlightsOff();
		GameObject target = character.gameObject;

		StartProjectileAnimation(target.transform.position);
		attacking = true;
	}

	protected void StartProjectileAnimation(Vector3 targetFinalPosition) {
		GetComponent<SpriteRenderer>().enabled = true;
		this.transform.position = AttackingCharacter.transform.position;
		this.gameObject.LookAt2D(targetFinalPosition);
		Vector2 forceDirection = (targetFinalPosition - this.transform.position).normalized;
		this.GetComponent<Rigidbody2D>().AddForce(forceDirection * projectileForce);
	}

	public void OnCollisionEnter2D(Collision2D collisionInfo) {
		Destroy(this.gameObject, 0.1f);
		Character characterCollided = collisionInfo.collider.GetComponent<Character>();
		if(characterCollided!=null){
			characterCollided.DecreaseLife(damage);
		}
//		Physics2D.IgnoreCollision(this.collider2D, collisionInfo.gameObject.collider2D);
	}

	//TODO: The trigger should have a script with a method to change whatever passes through.
	public void OnTriggerEnter2D(Collider2D other) {
		/*TODO: Find a way to understand what trigger triggered the method. Based on the trigger (es. Fire) and the attack(es. ArrowShot),
		 *	 	do different things.  */
		gameObject.GetComponent<SpriteRenderer>().color = Color.red;
	}

	//Disable
	public void OnDestroy(){
		grid.HighlightsOff();
		EndAction();
	}

	protected override bool SameTypeAs(object o){
		ProjectileAtt param = o as ProjectileAtt;
		return (param!=null);
	}

	protected override void Undo ()
	{
		throw new System.NotImplementedException ();
	}

}
