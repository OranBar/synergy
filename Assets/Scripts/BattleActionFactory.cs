﻿using UnityEngine;
using System.Collections; 
using System.Collections.Generic;

public class BattleActionFactory : MonoBehaviour, IBattleActionFactory {

	public List<GameObject> actionsList;

	public BattleAction CreateBattleAction( string actionName, AbsCharacter actingCharacter ) {
		foreach( GameObject action in actionsList ){
			if( action.name.Equals( actionName ) ){
				GameObject newAction = Instantiate( action );
				newAction.GetComponent<BattleAction>().Init(actingCharacter);
				return newAction.GetComponent<BattleAction>();
			}
		}
		throw new BattleActionNotFoundException(actionName);
	}


}
