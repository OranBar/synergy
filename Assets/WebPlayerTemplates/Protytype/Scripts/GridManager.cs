﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class GridManager : MonoBehaviour {

	public Camera mainCamera;

	public GameObject Tiles;

	//Singleton 
	private static GridManager gridManager;
	public static GridManager GetGridManager(){
		if(gridManager == null){
			gridManager = GameObject.FindObjectOfType<GridManager>();
		}
		return gridManager;
	}
// -----------------------------------------------------------------------------------
	private Tile[,] gridMatrix;
	public int rowLength, columnLength;
//	private int rows, columns;

	public GameObject tilePrefab;

	void Awake() {
		gridManager = GetGridManager();
		CreateGrid();
	}

	void Start () {

	}
	
	void Update () {
		
	}

	public Tile GetTile(int xIndex, int yIndex){
		return gridMatrix[xIndex, yIndex];
	}

	public void CreateGrid(){
		gridMatrix = new Tile[rowLength,columnLength];

		for(int y=0; y<columnLength; y++){
			for(int x=0; x<rowLength; x++){
				float middleX = Mathf.Floor(rowLength/2);
				float middleY = Mathf.Floor(columnLength/2);

				Tile tile = ((GameObject)Instantiate(tilePrefab, new Vector3(x - middleX , y - middleY, 0), Quaternion.identity )).GetComponent<Tile>();
				tile.transform.parent = Tiles.transform; 
				gridMatrix[x,y] = tile;
				tile.Init(x,y);
			}
		}
		mainCamera.orthographicSize = 7*rowLength / 9; //Best for square grids
	}

	public List<Tile> GetAdjacentTiles(Tile tile){
		List<Tile> adjacentTiles = new List<Tile>();
		int x = tile.xIndex;
		int y = tile.yIndex;
		if(x-1 >= 0){
			adjacentTiles.Add(gridMatrix[x-1 , y]);
		}
		if(x+1 <= columnLength-1){
			adjacentTiles.Add(gridMatrix[x+1 , y]);
		}
		if(y-1 >= 0){
			adjacentTiles.Add(gridMatrix[x , y-1]);
		}
		if(y+1 <= rowLength-1){
			adjacentTiles.Add(gridMatrix[x , y+1]);
		}
		return adjacentTiles;
	}

	public void HighlightMoveRange(Tile startTile, int range){
		if(startTile.movable){
			startTile.MoveHighlightOn(); 
		}
		if(range==0){
			return;
		} else {
			List<Tile> adjacentTilesList = GetAdjacentTiles(startTile);
			foreach(Tile adjacentTile in adjacentTilesList){
				HighlightMoveRange(adjacentTile, range-1);
			}
		}
	}

	public void HighlightAttackRange(Tile startTile, int range){
		startTile.AttackHighlightOn();
		if(range==0){
			return;
		} else {
			List<Tile> adjacentTilesList = GetAdjacentTiles(startTile);
			foreach(Tile adjacentTile in adjacentTilesList){
				HighlightAttackRange(adjacentTile, range-1);
			}
		}
	}


	public void HighlightsOff(){
		foreach(Tile tile in gridMatrix){
			tile.HighlightOff();
		}
	}


	#region Compute Line Drawing between two tiles. 

	public List<Tile> GetLine(Tile tile1, Tile tile2){
		List<point> linePoints = new List<point>();
		point point1 = new point(tile1.xIndex, tile1.yIndex);
		point point2 = new point(tile2.xIndex, tile2.yIndex);
		
		float diagonalDistance = Mathf.Round(DiagonalDistance (point1, point2));
		for(int i=0; i <= diagonalDistance; i++){
			float fattoreC = (diagonalDistance==0)? 0.0f : i/diagonalDistance;
			linePoints.Add (RoundPoint(LerpPoint(point1, point2, fattoreC)));
		}
		return ConvertPointstoTiles(linePoints);
	}

	private struct point{
		public float x,y;

		public point(float x, float y){
			this.x = x;
			this.y = y;
		}
	}

	private float Lerp(float start, float end, float fattoreC){
		return start + fattoreC * (end-start);
	}

	private point LerpPoint(point point1, point point2, float fattoreC){
		return new point(Lerp (point1.x, point2.x, fattoreC), Lerp (point1.y, point2.y, fattoreC));
	}

	private float DiagonalDistance(point point1, point point2){
		float distanceX = Mathf.Abs( point1.x - point2.x );
		float distanceY = Mathf.Abs( point1.y - point2.y );
		return Mathf.Max(distanceX, distanceY );
	}

	private point RoundPoint(point point){
		return new point(Mathf.Round(point.x), Mathf.Round(point.y));
	}



	private List<Tile> ConvertPointstoTiles(List<point> pointList){
		List<Tile> tileList = new List<Tile>();
		foreach(point currentPoint in pointList){
			Tile currentTile = gridMatrix[(int)currentPoint.x, (int)currentPoint.y];
			tileList.Add(currentTile);
		}
		return tileList;
	}
	#endregion

/*
	public void HighlightAdjacentTiles(Tile tile){
		List<Tile> adjacentTilesList = GetAdjacentTiles(tile);
		foreach(Tile adjacentTile in adjacentTilesList){
			adjacentTile.HighlightOn();	//Which highlight?
		}
	}
*/




}
