using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OranUnityUtils
{
	public static class TransformExtensionMethod 
	{
		
		public static IEnumerator SmoothTranslate(this Transform objTransform, Vector3 finalPosition, float smoothness = 0.005f, float duration = 0.45f, float accuracy=0.02f){
			Vector3 startPosition = objTransform.position;
			float progress = 0; 
			float increment = smoothness/duration; 
			
			while(progress<1 && Vector3.Distance(objTransform.position, finalPosition) > accuracy ){
				objTransform.position = Vector3.Lerp(startPosition, finalPosition, progress);
				progress +=increment;
				yield return new WaitForSeconds(smoothness);
			}
			objTransform.position = finalPosition;
		}
		
		public static IEnumerator SmoothRotationTranslate(this Transform objTransform, Quaternion finalRotation, float smoothness = 0.005f, float duration = 0.45f, float angleAccuracy=0.1f){
			Quaternion startRotation = objTransform.rotation;
			float progress = 0; 
			float increment = smoothness/duration; 
			
			while(progress<1 && Quaternion.Angle(objTransform.rotation, finalRotation) > angleAccuracy){
				objTransform.rotation = Quaternion.Lerp(startRotation, finalRotation, progress);
				progress +=increment;
				yield return new WaitForSeconds(smoothness);
			}
			objTransform.rotation = finalRotation;
		}

		public static Transform[] GetLineageTransforms(this Transform transf){
			Transform[] transforms = transf.GetComponentsInChildren<Transform>();
			return transforms;
		}
		
		public static Transform GetChild(this Transform transf, string name){
			Component[] transforms = transf.GetComponentsInChildren(typeof(Transform), true);
			
			foreach( Transform t in transforms ){
				if( t.gameObject.name == name )	{
					return t;
				}
			}
			return null;
		}
		
		public static List<Transform> GetParents(this Transform transform){
			List<Transform> parents = new List<Transform>();
			Transform temp = transform;
			while(temp.parent != null){
				temp = temp.parent;
				parents.Add(temp);
			}
			return parents;
		}	
	}
	
}