using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Player : MonoBehaviour
{

	private ActionPlanner actionPlanner;
	private List<GameAction> actionsHistory;
	private int currentActionIndex;

	public void DoTurn(GameState grid){
		actionPlanner.Activate(grid);
		//wait for answer
	} 

	public void EndTurn(){
		actionPlanner.Deactivate();
	}

	public void DoAction(GameAction action){
		action.Do();
		actionsHistory.Add(action);
		currentActionIndex++;
	}

	public void UndoAction(){
		//Check if action is undoable
		actionsHistory[currentActionIndex].Undo();
		currentActionIndex--;

	}


}

