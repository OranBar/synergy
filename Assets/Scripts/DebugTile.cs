﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class DebugTile : MonoBehaviour {

	public bool TileMouseOverDebug;

	// Use this for initialization
	void Start () {
	/*
		Tile[] tileScripts = GameObject.FindObjectsOfType(typeof(Tile)) as Tile[];
		foreach(Tile current in tileScripts){
			current.mouseOverCoordinates = TileMouseOverDebug;
		}
	*/
	}
	

	void Update () {
		Tile[] tileScripts = GameObject.FindObjectsOfType(typeof(Tile)) as Tile[];
		foreach(Tile current in tileScripts){
			current.mouseOverCoordinates = TileMouseOverDebug;
		}
	}
}
