﻿using UnityEngine;
using System.Collections;

public class FreezeRotation : MonoBehaviour {

	public bool freezeX, freezeY, freezeZ;

	void LateUpdate () {
		float x = (freezeX)? 0f : transform.rotation.x;
		float y = (freezeY)? 0f : transform.rotation.y;
		float z = (freezeZ)? 0f : transform.rotation.z;

		gameObject.transform.rotation = Quaternion.Euler(x,y,z);
	}
}
