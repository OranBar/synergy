﻿using UnityEngine;
using System.Collections;

public class UIscript : MonoBehaviour{

	public CharacterUser player;

	public void UseArrow(){
		player.UseArrow();
	}

	public void UseFire(){
		player.UseFire();
	}

	public void UseEarthWall(){
		player.UseEarthWall();
	}

	public void DefendEarthWall(){
		player.DefendEarthWall();
	}


}
