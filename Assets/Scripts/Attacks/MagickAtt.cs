using System;
using UnityEngine;

public class MagickAtt : ProjectileAtt {

	private GameObject target;

	protected override void AttackTile(Tile tile) {
		target = tile.gameObject;

		//Don't use physics if you are not going to collide with anything. Use smoothTranslate instead, from OranUnityUtils
		base.StartProjectileAnimation(target.transform.position);
	}

	public void FixedUpdate() {
		if(!attacking){ return; }

		if (ProjectileArrivedToDestination ()) {
			//TODO: BUG HERE
			Debug.Log("Trying to stop attack");
			this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			this.transform.position = target.transform.position;
		}
	}

	
	private bool ProjectileArrivedToDestination() {
		return Vector3.Distance(this.transform.position, target.transform.position) <= 0.5f;
	}
	
	
}