using UnityEngine;
using System.Collections;

public class PlayerTurn : State {
	
	public PlayerTurn(StateMachine stateManager) : base(stateManager){
		
	}

	public override void OnTileClick(Tile tilePressed){

	}
	
	public override void OnCharacterClick(AbsCharacter characterPressed){
		Log("Character clicked");

		selectedCharacter = characterPressed;
		stateManager.SwitchState(new CharacterSelected(stateManager));
	}

	protected override string GetStateName ()
	{
		return "PlayerTurn";
	}

}
