using System;
using System.Runtime.Serialization;

public class BattleActionNotFoundException : Exception {


	public BattleActionNotFoundException() : base("Battle Action not found") {

	}

	public BattleActionNotFoundException(string actionNotFoundName) : base("Battle Action called "+actionNotFoundName+" not Found"){
		
	}

	public BattleActionNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context){

	}
}

