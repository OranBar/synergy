﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	//Lazy Singleton	
	private static GameManager gameManager;
	public static GameManager GetGameManager(){
		return gameManager;
	}

	void Awake(){
		gameManager = this;
		battleActionFactory = GameObject.FindObjectOfType<BattleActionFactory>();
	}

	void Start(){

	}

	public delegate void CharacterSelected();
	public event CharacterSelected OnCharacterSelected;

	private Character _selectedCharacter;
	public Character SelectedCharacter {
		get{
			return _selectedCharacter;
		}
		set{
			_selectedCharacter = value;
			if(OnCharacterSelected!=null){
				OnCharacterSelected();
			}
			//Shouldn't be done here
		//	_selectedCharacter.ShowMovableTiles();
		}
	}

	private BattleActionFactory battleActionFactory;

	public void ButtonPressed(string attackName){
		DebugEx.PrintMethodName();
		SelectedCharacter.DoAction( battleActionFactory.CreateBattleAction (attackName, SelectedCharacter) );
		// SelectedCharacter.InstantiateAttack(attackName);
	}
}
