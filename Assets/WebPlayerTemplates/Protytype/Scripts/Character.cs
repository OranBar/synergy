﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum CharState {
	Moving, Attacking, Waiting
}

public class Character : MonoBehaviour {

	public static int availableId=0;

	private GridManager gridManager;

	public int life = 100;
	public int myId { get; private set;}
	public int xIndex { get; private set; }
	public int yIndex { get; private set; }
	public Tile currentTile{ get; set; }
	private Tile destinationTile;
	public int moveRange=5;
	public int attackRange=5;
	protected bool move=false;
	protected bool charge=false;
	public CharState charState { get; set; }
	private TurnManager turnManager;

	public GameObject defenseButton;
	private Character attacker;
	public GameObject earthWall;
	public float movementSpeed = 10f;
	public float chargeSpeed = 17f;

	private Character currentTarget;
	private bool projectileOnFire;	//osceno

	public void Init(Tile currentTile){
		this.myId = availableId;
		availableId++;
		this.currentTile = currentTile;
		this.xIndex = currentTile.xIndex;
		this.yIndex = currentTile.yIndex;
		gridManager = GridManager.GetGridManager();
		this.turnManager = TurnManager.GetTurnManager();
	}
	
	public void Init(int xIndex, int yIndex){
		this.myId = availableId;
		availableId++;
		this.xIndex = xIndex;
		this.yIndex = yIndex;
		gridManager = GridManager.GetGridManager();
		this.currentTile = gridManager.GetTile(xIndex, yIndex);
		this.turnManager = TurnManager.GetTurnManager();

	}

	public virtual void DecreaseLife(int damage){
		life = life - damage;
		Debug.Log("Life was decreased by "+damage+"\nNow life is "+life+"//100");
		if(life<=0){
			this.currentTile.movable = true;
			Death();
		}
	}

	protected virtual void Death(){
		gameObject.SetActive(false);
	}

	
	public void Move(Tile destinationTile){
		currentTile.movable = true;
		move=true;
		this.destinationTile = destinationTile;
		destinationTile.movable = false;
		destinationTile.charOnTile = this;
		TurnManager.GetTurnManager().NextTurn(this);
	}

	//TODO: liberarsi di questo metodo, creato solo per test. 
	//TODO: gli attacchi sono classi, e contengono un metodo showAnimation();

	public void Attack(Character target, bool projectileOnFire, string attackName){
		this.projectileOnFire = projectileOnFire;
		attackName.ToLower ();
		if(attackName.Contains("arrow")){
			ArrowAnimation (target);
		}
		if(attackName.Contains("charge")){
			ChargeAnimation (target);
		}
		TurnManager.GetTurnManager().NextTurn(this);
	}

	public void ArrowAnimation (Character target){
		this.GetComponent<ArrowSpanwer> ().target = target.gameObject;
		this.GetComponent<ArrowSpanwer> ().ShootTrue ();
	}

	public void ChargeAnimation(Character target){
		this.currentTarget=target;
		charge = true;
		List<Tile> tileList = gridManager.GetLine(currentTile, target.currentTile);
		destinationTile = tileList[tileList.Count-2];
	}

	public void OnCollisionEnter2D(Collision2D collisionInfo){
		if(collisionInfo.collider.name.Contains("rrow")){
			if(collisionInfo.gameObject.GetComponent<SpriteRenderer>().color == Color.red){
				Debug.Log ("The arrow was on fire");
				DecreaseLife(20);
			}else{
				Debug.Log ("The arrow was not on fire");
				DecreaseLife(10);
			}
		}
	}

	public void AllowDefense(Character attacker){
		this.attacker = attacker;
		if(attacker is CharacterAI){
			if(turnManager.userTeamActions>0){
				defenseButton.SetActive(true);
				new WaitForSeconds(3.0f);
				Debug.Log("waiting 3 WaitForSeconds");
				defenseButton.SetActive(false);
			}
		}
		if(attacker is CharacterUser){
			if(turnManager.aiTeamActions>0){
				// Defend
			}
		}
	}

	public void DefendEarthWall(){
		Tile tile = gridManager.GetLine(currentTile, attacker.currentTile)[1];
		tile.wallOnTile = true;
		Instantiate(earthWall, tile.transform.position, Quaternion.identity);
		turnManager.NextTurn(this);
		defenseButton.SetActive(false);
	}

	public void ActivateDefenseButton(bool boolean){
		if(turnManager.userTeamActions>0){
			defenseButton.SetActive(boolean);
		}
	}


	void Start () {

	}
	
	virtual internal void Update () {
		if(move){
			transform.position = Vector3.Lerp(transform.position, destinationTile.transform.position, movementSpeed * Time.deltaTime);
			if( Vector3.Distance(transform.position, destinationTile.transform.position) <= 0.01f ){
				move=false;
				currentTile = destinationTile;	//Maybe I can take this out
			}
		}
		if(charge){
			transform.position = Vector3.Lerp(transform.position, destinationTile.transform.position, chargeSpeed * Time.deltaTime);
			if( Vector3.Distance(transform.position, destinationTile.transform.position) <= 0.01f ){
				Debug.Log ("mehere");
				currentTile.movable = true;
				currentTile = destinationTile;
				destinationTile.movable = false;
				this.renderer.material.color = Color.white;
				charge=false;
				Debug.Log("Charging");
				if(projectileOnFire){
					currentTarget.DecreaseLife(30);
				}else{
					currentTarget.DecreaseLife(25);
				}
			}
		} 
	}
}
