﻿using UnityEngine;
using System.Collections;

public class CharacterAiBasic : Character {

	public Attack GetRandomAttack(){
		int randomAttackIndex = Random.Range(0, usableAttacks.Count);
		return usableAttacks[randomAttackIndex];
	}

}
