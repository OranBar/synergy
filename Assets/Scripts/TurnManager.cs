using System;
using UnityEngine;

public enum Turn{
	Player, Enemy
}

public class TurnManager : MonoBehaviour {

	//Lazy Singleton
	private static TurnManager turnManager;
	public static TurnManager GetTurnManager(){
		return turnManager;
	}


	public int maxPlayerActions;
	public int maxEnemyActions;
	public int playerActions, enemyActions;
	public Turn CurrentTurn {get;set;}

	private StateMachine stateManager;


	public void Awake(){
		turnManager = this;
	}

	public void Start(){
		stateManager = GameObject.FindObjectOfType(typeof(StateMachine)) as StateMachine;
		playerActions = maxPlayerActions;
		enemyActions = maxEnemyActions;
	}

	public int RemainingActions{
		get{
			if(CurrentTurn == Turn.Player){
				return playerActions;
			}
			else if(CurrentTurn == Turn.Enemy){
				return enemyActions;
			}
			return -1; //Error
		}
		set{
			if(CurrentTurn == Turn.Player){
				playerActions = value;
			}
			else if(CurrentTurn == Turn.Enemy){
				enemyActions = value;
			}
		}
	}

	public void DecreaseRemainingActions(int actionCost = 1){
		if(CurrentTurn == Turn.Player){
			for(int i=0; i< actionCost; i++){
				playerActions--;
			}
			if(RemainingActions<=0){
				StartNewTurn(Turn.Enemy);
			} else {
				stateManager.SwitchState( new PlayerTurn(stateManager) );
			}
		}
		else if(CurrentTurn == Turn.Enemy){
			for(int i=0; i< actionCost; i++){
				enemyActions--;
			}
			if(RemainingActions<=0){
				StartNewTurn(Turn.Player);
			} else {
				stateManager.SwitchState( new EnemyTurn(stateManager) );
			}
		}
	}



	public void StartNewTurn(Turn turn){
		CurrentTurn = turn;			
		Debug.Log("Now "+CurrentTurn+" turn" );

		if(CurrentTurn == Turn.Player){
			RemainingActions = maxPlayerActions;
			stateManager.SwitchState(new PlayerTurn(stateManager));
		}
		else if(CurrentTurn == Turn.Enemy){
			RemainingActions = maxEnemyActions;
			stateManager.SwitchState(new EnemyTurn(stateManager));

		}
	}


}


