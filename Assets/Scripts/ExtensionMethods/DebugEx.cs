﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;


public class DebugEx : MonoBehaviour {

	public static void PrintMethodName(){
		StackTrace st = new StackTrace ();
		StackFrame sf = st.GetFrame (1);

		UnityEngine.Debug.Log(sf.GetMethod().Name);
	}
}
