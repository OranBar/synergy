using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OranUnityUtils;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class MoveAction : BattleAction
{

//	private AbsCharacter usingCharacter;
	private Grid grid;
	private SpriteController spriteController;
	private StateMachine stateMachine;

	private List<Tile> movableTiles;

	/*
	public MoveAction(AbsCharacter character, StateMachine sm) : base(sm){
		grid = Grid.GetGrid();
		usingCharacter = character;
		spriteController = usingCharacter.GetComponent<SpriteController>();
	}
	*/

	public override void InitImpl() {
		grid = FindObjectOfType<Grid>();
		UsingCharacter = GameManager.GetGameManager().SelectedCharacter;
		stateMachine = GameObject.FindObjectOfType<StateMachine>();

		stateMachine.SwitchState(new CharacterActing(stateMachine, this));
		Log("Move Action activated");
		spriteController = UsingCharacter.GetComponent<SpriteController>();
		Log("Character "+UsingCharacter.name+" showing movable tiles");
		//UsingCharacter.ShowMovableTiles();
		HightlightMovableTiles();
	}

	public override void OnTileClick (Tile tile) {
		Log("Move Action Tile Click");

		if(ComputeMovableTiles().Contains(tile)==false){
		//if ((HighlightColors.Move.Equals(tile.CurrentHighlight) == false) ) {
			//TODO: Change the if check to something more relaiable than highlight tile. 
			// This way triggers the exception when enemy tries to move, since it does not highlight

			if(TurnManager.GetTurnManager().CurrentTurn == Turn.Player){
				throw new System.Exception("Tile not in movement range");
			}
			
			return;
		}

		UsingCharacter.CurrentTile.CharacterOnTile = null;

		List<Tile> path = grid.A_StarPathfinding( UsingCharacter.CurrentTile, tile );

		//spriteController.Move(path);
		//spriteController.StartCoroutineTimeline(
		//	spriteController.Move_Coro( path, spriteController.moveSpeed ),
		//	this.ToIEnum( (()=> grid.HighlightsOff()) )
		//);

		grid.HighlightsOff();
		spriteController.Move(path);

		UsingCharacter.CurrentTile = tile.GetComponent<Tile>();
		UsingCharacter.CurrentTile.CharacterOnTile = UsingCharacter.gameObject;
		UsingCharacter.CurrentTile.Movable = false;
		Log("Move Action End");
		EndAction();
	}

	public override void OnCharacterClick(AbsCharacter character) {
		Log("Move Action Character click");
	}

	public override string GetActionName() {
		return "Move";
	}

	protected override bool SameTypeAs (object o){
		MoveAction param = o as MoveAction;
		return (param != null);
	}

	protected override void Undo ()
	{
		throw new System.NotImplementedException ();
	}

	protected virtual void HightlightMovableTiles(){
		List<Tile> movableTiles = ComputeMovableTiles();
		grid.HighlightTiles(movableTiles, HighlightColors.Move);
	}

	private List<Tile> ComputeMovableTiles(){
		return grid.DjikstraRadius(UsingCharacter.CurrentTile, UsingCharacter.MoveRadius);
	}






}

